#include <QApplication>
#include <time.h>

#include "tp2.h"

int value;
MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// selectionSort

    for (int j=0; j<toSort.size();j++){
        for (int i = j; i<toSort.size(); i++){
            if(toSort[j]>toSort[i]){
                int temp = toSort[j];
                toSort[j]=toSort[i];
                toSort[i]=temp;
            }

        }
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
