#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if(origin.size()<2){
        return;
    }
	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
        for (int i=0; i<origin.size()/2; i++){
            first[i]=origin[i];
        }
        for (int j=0; j<origin.size()-first.size(); j++){
            second[j]=origin[j+origin.size()/2];
        }
	// recursiv splitAndMerge of lowerArray and greaterArray
        splitAndMerge(first);
        splitAndMerge(second);
	// merge
        merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{

    int i =0;
    int j = 0;
    int k =0;

while (result.size() > k)
{
    if (i == first.size())
    {
        result[k]=second[j];
        j++;
        k++;

    }
    else if (j == second.size()){
        result[k]=first[i];
        i++;
        k++;

    }

    else if (first[i]<second[j])
    {
        result[k]=first[i];
        i++;
        k++;

    }

    else
    {
        result[k]=second[j];
        j++;
        k++;
    }
}

}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
