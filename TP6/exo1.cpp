#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */

    for(int i = 0; i < nodeCount; i++)
    {
        GraphNode* graphnoeud = new  GraphNode(i);
        this->appendNewNode(graphnoeud);
    }
    for(int j = 0; j < nodeCount; j++)
    {
        for(int k = 0; k<nodeCount; k++)
        {
            if(adjacencies[j][k] != 0)
            {
                this->nodes[j]->appendNewEdge(nodes[k],adjacencies[j][k]);
            }
        }
    }
    return;
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
    nodes[nodesSize] = first;
    nodesSize ++;
    visited[first->value] =  true;
    for(Edge* e=first->edges; e!=NULL; e=e->next)
    {
        if(!visited[e->destination->value])
        {
            deepTravel(e->destination, nodes, nodesSize, visited);
        }
    }
    return;
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first);

    while (!nodeQueue.empty())
    {
        GraphNode* nd = nodeQueue.front();
        nodes[nodesSize] = nd;
        nodeQueue.pop();
        nodesSize++;
        visited[nd->value] = true;
        for (Edge* e = first->edges; e != nullptr; e = e->next)
        {
            if (!visited[e->destination->value])
            {
                nodeQueue.push(e->destination);
            }
        }
    }
    return;
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
    visited[first->value]=true;

        for (Edge* e = first->edges; e!=NULL; e=e->next)
        {
            if (!visited[e->destination->value])
            {
                if (detectCycle(e->destination, visited))
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
