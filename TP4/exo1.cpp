#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

const int doubleIndex = 2;
const int doubleDiv = 2;

int Heap::leftChild(int nodeIndex)
{

    return nodeIndex * doubleIndex + 1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex * doubleIndex + 2;
}


void Heap::insertHeapNode(int heapSize, int value)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i = heapSize;
    (*this)[i]=value;
    while(i>0 && (*this)[i] > (*this)[(i-1)/doubleDiv]){
        this->swap(i,(i-1)/doubleDiv);
        i=(i-1)/doubleDiv;
    }

    return;
}

void Heap::heapify(int heapSize, int nodeIndex)
{

    // use (*this)[i] or this->get(i) to get a value at index i
    int i_max = nodeIndex;
    if(this->leftChild(nodeIndex) < heapSize && (*this)[i_max] < (*this)[this->leftChild(nodeIndex)]){
        i_max = this->leftChild(nodeIndex);
    }
    if(this->rightChild(nodeIndex) < heapSize && (*this)[i_max] < (*this)[this->rightChild(nodeIndex)]){
        i_max = this->rightChild(nodeIndex);
    }
    if(i_max != nodeIndex){
        this->swap(nodeIndex, i_max);
        this->heapify(heapSize, i_max);
    }
    return;
}

void Heap::buildHeap(Array& numbers)
{
        for(int i=0; i<numbers.size(); i++){
            insertHeapNode(numbers.size(), numbers[i]);
        }
        return;
}

void Heap::heapSort()
{
    for (int i = (this->size()-1); i > 0; i--) {
        this->swap(0, i);
        this->heapify(i, 0);
    }
    return;

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
    w->show();

    return a.exec();
}
