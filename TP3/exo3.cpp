#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        this->left = nullptr;
        this->right = nullptr;
        this->value = value;
        // init initial node without children
    }

	void insertNumber(int value) {
        if(value < this->value){
            if(this->left == nullptr){
                this->left = createNode(value);
            }
            else {
                this->left->insertNumber(value);
            }

        }
        else {
            if(this->right == nullptr){
                this->right = createNode(value);

            }
            else {
                this->right->insertNumber(value);
            }
        }
        // create a new node and insert it in right or left child
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint leftChild = 0;
        uint rightChild = 0;

        if(this->left !=0){
            leftChild = this->left->height();
        }
        else {
            leftChild = 0;
        }

        if(this->right !=0){
            rightChild = this->right->height();
        }
        else {
            rightChild = 0;
        }

        if (rightChild < leftChild ){
            return leftChild +1;
        }
        else{
            return rightChild +1;
        }
        return 1;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        return 1;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
	}

	Node* find(int value) {
        // find the node containing value
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
